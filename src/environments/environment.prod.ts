export const environment = {
  production: true,
  authToken: 'Bearer 577353a9879f0e04596b9ffdc1b28eb977c3f9bec3ea0a85f2ace6b0eab1d6b1',
  baseUrl: 'https://api.youneedabudget.com/v1',
  budgetUrl: 'https://api.youneedabudget.com/v1/budgets'
};
