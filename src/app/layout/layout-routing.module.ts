import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';

const route: Routes = [
    {
        path: '', component: LayoutComponent,
        children : [
            { path: '' , redirectTo: '/budget', pathMatch: 'full'},
            { path: 'budget' , loadChildren: './budget/budget.module#BudgetModule' },
            { path: 'account' , loadChildren: './accounts/accounts.module#AccountsModule' }
        ]
    }
]

@NgModule({
    imports: [
        RouterModule.forChild(route)
    ],
    exports: [
        RouterModule
    ],
    declarations: [

    ]
})

export class LayoutRoutingModule {

}