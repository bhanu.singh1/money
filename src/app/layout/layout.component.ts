import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  currentTab = '/budget';
  budgetCount = 0;

  constructor(
    private router: Router
  ) {
    this.currentTab = this.router.url;
  }

  ngOnInit() {

  }

  tabclick(path: string) {
    this.currentTab = path;
    this.router.navigate([path]);
  }

}
