interface BudgetList {
    name: string;
    first_month: string;
    last_month: string;
    last_modified_on: string;
}