import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BudgetRoutingModule } from './budget-routing.module';
import {
  BudgetDetailComponent,
  BudgetListComponent
} from './components';

@NgModule({
  declarations: [
    BudgetListComponent,
    BudgetDetailComponent
  ],
  imports: [
    CommonModule,
    BudgetRoutingModule
  ],
  providers:[
  ]
})
export class BudgetModule { }
