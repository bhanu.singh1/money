import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/layout/shared/service/common.service';

@Component({
  selector: 'app-budget-list',
  templateUrl: './budget-list.component.html',
  styleUrls: ['./budget-list.component.scss']
})
export class BudgetListComponent implements OnInit {

  budgetList:Array<BudgetList> = [];
  errMsg:string = '';

  constructor(
    private service: CommonService
  ) { }

  ngOnInit() {
    this.getBudgetList();
  }

  getBudgetList() {
    this.service.getBudgetList()
    .subscribe(
      res => {
        if(res && res['data'] && res['data']['budgets']) {
          this.budgetList = res['data']['budgets'];
          this.budgetList.map(e => e['toggle'] = false);
          localStorage.setItem('budgetID', res['data']['budgets'][0]['id'].toString())
        }
      },
      error => {
        this.errMsg = error['error']['details'];
      }
    )
  }
}
