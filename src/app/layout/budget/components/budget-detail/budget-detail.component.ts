import { Component, Input, OnInit } from '@angular/core';
import { CommonService } from 'src/app/layout/shared/service/common.service';

@Component({
  selector: 'app-budget-detail',
  templateUrl: './budget-detail.component.html',
  styleUrls: ['./budget-detail.component.scss']
})
export class BudgetDetailComponent implements OnInit {

  @Input() bidID: string;
  budgetDetail: Array<AccountList> = [];
  errMsg: string = '';

  constructor(
    private service: CommonService
  ) {}

  ngOnInit() {
   this.getBudgetDetails();
  }

  getBudgetDetails() {
    this.service.getBudgetDetail(this.bidID).subscribe(
      res => {
        if (res && res['data'] && res['data']['budget']) {
          this.budgetDetail = res['data']['budget'];
        }
      },
      error => {
        this.errMsg = error['error']['details'];
      }
    )
  }
}
