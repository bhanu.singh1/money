import { NgModule } from '@angular/core';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './core';
import { CommonModule } from '@angular/common';
import { CommonService } from './shared/service/common.service';

@NgModule({

    imports: [
        LayoutRoutingModule,
        CommonModule
    ],
    exports: [

    ],
    declarations: [
        LayoutComponent,
        HeaderComponent
    ],
    providers: [
        CommonService
    ]
})

export class LayoutModule {

}