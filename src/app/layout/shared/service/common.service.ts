import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../../environments/environment";
import { catchError, retry, share } from 'rxjs/operators';
import { of } from "rxjs";

@Injectable()

export class CommonService {

    headers = new HttpHeaders();

    constructor(
        private http: HttpClient
    ) {
        this.headers = this.headers.append('content-type', 'application/json');
        this.headers = this.headers.append('Authorization', environment.authToken);
    }

    getBudgetList() {
        const url = environment.baseUrl;

        return this.http.get(url,
            {
                headers: this.headers,
                responseType: 'json'
            })
            .pipe(
                share(),
                retry(3),
                catchError(err => {
                    return of(err);
                })
            )
    }

    getBudgetDetail(budgetId) {
        const url = `${environment.budgetUrl}/${budgetId}`;

        return this.http.get(url,
            {
                headers: this.headers,
                responseType: 'json'
            })
            .pipe(
                share(),
                retry(3),
                catchError(err => {
                    return of(err);
                })
            )

    }
}