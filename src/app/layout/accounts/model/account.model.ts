interface AccountList {
    name: string;
    balance: number;
    cleared_balance: number;
    type: string;
    note: string;
} 