import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountsRoutingModule } from './accounts-routing.module';
import { ListComponent } from './components';

@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    CommonModule,
    AccountsRoutingModule
  ],
  providers: [

  ]
})
export class AccountsModule { }
