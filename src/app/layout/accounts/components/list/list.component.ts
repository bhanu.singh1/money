import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/layout/shared/service/common.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  acountList: Array<AccountList> = [];
  errMsg: string = '';
  budgetID: string = localStorage.getItem('budgetID');

  constructor(
    private service: CommonService
  ) {}

  ngOnInit() {
    if(this.budgetID) {
      this.getAccountList();
    }
  }


  getAccountList() {
    this.service.getBudgetDetail(this.budgetID).subscribe(
      res => {
        if (res && res['data'] && res['data']['budget']) {
          this.acountList = res['data']['budget'];
        }
      },
      error => {
        this.errMsg = error['error']['details'];
      }
    )
  }

}
